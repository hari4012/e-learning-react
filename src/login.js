import React, { useState } from "react";
import './login.css'
import back from "./images/background.png";
import logo from "./images/logo.png";
import Main from "./main.js";
;




function Login(){
    
    
    const [errorMessages, setErrorMessages] = useState({});
  const [isSubmitted, setIsSubmitted] = useState(false);

  // User Login info
  const database = [
    {
      username: "user1",
      password: "hari"
    },
    {
      username: "user2",
      password: "gopi"
    }
  ];

  const errors = {
    uname: "invalid username",
    pass: "invalid password"
  };

  const handleSubmit = (event) => {
    //Prevent page reload
    event.preventDefault();

    var { uname, pass } = document.forms[0];

    // Find user login info
    const userData = database.find((user) => user.username === uname.value);

    // Compare user info
    if (userData) {
      if (userData.password !== pass.value) {
        // Invalid password
        setErrorMessages({ name: "pass", message: errors.pass });
      } else {
        setIsSubmitted(true);
      }
    } else {
      // Username not found
      setErrorMessages({ name: "uname", message: errors.uname });
    }
  };

  // Generate JSX code for error message
  const renderErrorMessage = (name) =>
    name === errorMessages.name && (
      <div className="error">{errorMessages.message}</div>
    );

  // JSX code for login form
  const renderForm = (
    <div className="form">
      <form onSubmit={handleSubmit}>
          <div className="hello"><h2>Login to your account</h2></div>
        <div className="input-container">
          <label>Customer Id </label>
          <input type="text" placeholder="Enter your Id" name="uname" required />
          {renderErrorMessage("uname")}
        </div>
        <div className="input-container">
          <label>Password </label>
          <input type="password" placeholder="Enter your password" name="pass" required />
          {renderErrorMessage("pass")}
        </div>
        <div className="button-container">
        <button type="submit">Login</button>
        </div>
      </form>
      
    </div>
    
  );

  return (
    <div className="app">
        
           <div className="img"><img src={back} alt="meme"/>
          <div>{isSubmitted ? <div></div> : renderForm}</div> 
          <div className="logo"><img src={logo} alt="logo"/></div>
          
        <div className="wel"> <div className="net"> <h1>Welcome to</h1>
        <div className="crys"> <h1>Crystal delta</h1></div>
        <div className="yet"><h1>e-learning</h1></div></div></div>
        
       </div>
       
    
    </div>
    
  );
}

export default Login;