import React  from "react";
import reacts from "./images/reactjs.jpg";
import logo from "./images/logo.png";
import './dashboard.css';
const Dash = () =>{
    return(
        <div className="total">
        <div className="nav">
            <div className="logo"><img src={logo}></img>
            </div>
            <div className="menu">
                <ul>
                    <li><a href="home.html">All Courses</a></li>
                    <li><a href="#">My Dashboard</a></li>
                </ul>
            </div>
            <div className="logout">
                <button className="logbutton"><a href="index.html">Logout</a></button>
            </div>
        </div>
        <div className="content">
            <h1>Greatings Shyam,</h1>
            <h2>Take a look into the list of all courses</h2>
        </div>
        <div className="card1">
            <div className="image"><img src={reacts}></img></div>
            <div className="title">
                 <h2>React js Basics</h2> <br />
                 <h4>Course ID : CDL001</h4>
            </div>
            <div className="time1">
                <p> Start Date:<br />19-12-2020</p>
            </div>
            <div className="time2">
                <p>End Date:<br />15-06-2021 </p>
            </div>
        </div>
        </div>);
    }
export default Dash;
